<?php

use yii\db\Migration;

/**
 * Class m210505_071914_devlog
 */
class m210505_071914_devlog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'pk' => $this->bigPrimaryKey(),
            'usr' => $this->string()->notNull()->unique(),
            'pwd' => $this->string()->notNull(),
            'mail' => $this->string()->notNull(),
            'name' => $this->string()->null(),
            'created' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'updated' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
        $this->createIndex('users_index', 'users', ['usr', 'pwd']);

        $this->createTable('posts', [
            'pk' => $this->bigPrimaryKey(),
            'by' => $this->bigInteger()->notNull(),
            'head' => $this->string()->null(),
            'body' => $this->text()->null(),
            'created' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'updated' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
        $this->addForeignKey(
            'posts_users_fk',
            'posts',
            'by',
            'users',
            'pk',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210505_071914_devlog cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210505_071914_devlog cannot be reverted.\n";

        return false;
    }
    */
}
