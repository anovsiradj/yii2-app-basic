<?php

return [
	'adminEmail' => 'admin@example.com',
	'senderEmail' => 'noreply@example.com',
	'senderName' => 'Example.com mailer',
	'analytics' => [
		'developerKey' => '', // Public key fingerprints (prod is also empty)
		'serviceAccountName' => 'xxx@developer.gserviceaccount.com', // Email address
		'clientId' => 'xxx.apps.googleusercontent.com', // Client ID
		'analyticsId' => 'ga:xxx',
	],
];
