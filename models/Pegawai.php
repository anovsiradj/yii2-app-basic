<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pegawai".
 *
 * @property int $pk
 * @property int $pk_perusahaan
 * @property int $pk_departemen
 * @property int $pk_jabatan
 * @property string $nama
 *
 * @property Departemen $pkDepartemen
 * @property Jabatan $pkJabatan
 * @property Perusahaan $pkPerusahaan
 */
class Pegawai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pegawai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pk_perusahaan', 'pk_departemen', 'pk_jabatan', 'nama'], 'required'],
            [['pk_perusahaan', 'pk_departemen', 'pk_jabatan'], 'integer'],
            [['nama'], 'string'],
            [['pk_departemen'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['pk_departemen' => 'pk']],
            [['pk_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['pk_jabatan' => 'pk']],
            [['pk_perusahaan'], 'exist', 'skipOnError' => true, 'targetClass' => Perusahaan::className(), 'targetAttribute' => ['pk_perusahaan' => 'pk']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '#' => 'No',
            'pk' => 'Pk',
            'pk_perusahaan' => 'Perusahaan',
            'pkPerusahaan.nama' => 'Perusahaan',
            'pk_departemen' => 'Departemen',
            'pkDepartemen.nama' => 'Departemen',
            'pk_jabatan' => 'Jabatan',
            'pkJabatan.nama' => 'Jabatan',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[PkDepartemen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPkDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['pk' => 'pk_departemen']);
    }

    /**
     * Gets query for [[PkJabatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPkJabatan()
    {
        return $this->hasOne(Jabatan::className(), ['pk' => 'pk_jabatan']);
    }

    /**
     * Gets query for [[PkPerusahaan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPkPerusahaan()
    {
        return $this->hasOne(Perusahaan::className(), ['pk' => 'pk_perusahaan']);
    }
}
