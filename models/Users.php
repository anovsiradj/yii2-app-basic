<?php
/**
* model sekaligus "identityClass" (auth)
* 
* @see https://www.yiiframework.com/doc/guide/2.0/en/security-authentication
*/

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\validators\EmailValidator;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $pk
 * @property string $usr
 * @property string $pwd
 * @property string $mail
 * @property string|null $name
 * @property string $created
 * @property string $updated
 *
 * @property posts[] $posts
 */
class Users extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $new_pwd;
    public $new_pwd_repeat;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = $scenarios[self::SCENARIO_UPDATE] = array_merge($scenarios[self::SCENARIO_DEFAULT], [
            'new_pwd',
            'new_pwd_repeat',
        ]);
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'default'],
            [['usr','mail'], 'unique'],
            [['usr', 'mail'], 'required'],
            ['mail', EmailValidator::class],

            [['new_pwd', 'new_pwd_repeat'], 'required', 'on' => self::SCENARIO_CREATE],
            ['new_pwd_repeat', 'compare', 'compareAttribute' => 'new_pwd'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pk' => 'Pk',
            'usr' => 'Username',
            'pwd' => 'Password',
            'old_pwd' => 'Password Lama',
            'new_pwd' => 'Password Baru',
            'new_pwd_repeat' => 'Ulangi Password Baru',
            'mail' => 'Email',
            'name' => 'Nama',
            'created' => 'Dibuat Pada',
            'updated' => 'Diubah Pada',
        ];
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['by' => 'pk']);
    }

    public function validatePassword($pwd)
    {
        return true;
    }

    /* REQUIRED */
    /**
    * @see findIdentity()
    */
    public function getId()
    {
        return $this->pk;
    }

    public function getUsername()
    {
        return $this->usr;
    }
    public static function findByUsername($usr)
    {
        return self::findOne(['usr' => $usr]);
    }

    /* REQUIRED */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /* REQUIRED */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \Exception(__METHOD__ . ' is unimplemented', 1);
    }

    /* REQUIRED */
    public function getAuthKey()
    {
        /* return $this->authKey; */

        // mbuh kepie cara nggone, sementara ngene wae.
        return $this->usr;
    }
    /* REQUIRED */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}
