<?php

namespace app\controllers;

use yii\web\Controller;

class GoogleAnalyticsController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
