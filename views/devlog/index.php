<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Index';
$this->params['breadcrumbs'][] = ['label' => 'DevLog', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-index">
    <?php foreach ($list as $item):
        $href_view = Url::toRoute(['devlog/view', 'id' => $item->pk]);
        $item_name = $item->users->name ?? $item->users->usr;
        $href_name = Url::toRoute(['index', 'search[by]' => $item->users->id]);
        $item_created = $item->created;
        $item_created = Yii::$app->formatter->asDatetime($item_created);
        ?>
        <h1><a href="<?= $href_view ?>"><?= Html::encode($item->head) ?></a></h1>
        <p>
            Oleh
            <?= Html::a($item_name, $href_name) ?>
            Pada <?= $item_created ?>
        </p>
    <?php endforeach ?>
</div>
