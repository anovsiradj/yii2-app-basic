<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $pk
 * @property int $by
 * @property string|null $head
 * @property string|null $body
 * @property string $created
 * @property string $updated
 *
 * @property users $by0
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['by','head','body'], 'required'],
            [['by'], 'integer'],
            [['body'], 'string'],
            [['created', 'updated'], 'safe'],
            [['head'], 'string', 'max' => 255],
            [['by'], 'exist', 'skipOnError' => true, 'targetClass' => users::className(), 'targetAttribute' => ['by' => 'pk']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pk' => 'Pk',
            'by' => 'By',
            'head' => 'Head',
            'body' => 'Body',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['pk' => 'by']);
    }
}
