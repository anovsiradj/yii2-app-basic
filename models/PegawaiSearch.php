<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * summary
 */
class PegawaiSearch extends Pegawai
{
	use \app\classes\ModelSearch;

	/**
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params, ...$paramsExtras)
	{
		$query = Pegawai::find()->joinWith(['pkPerusahaan', 'pkDepartemen', 'pkJabatan']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		foreach ($this->attributes() as $attr) $query->andFilterWhere(["{$this->tableName()}.$attr" => $this->getAttribute($attr)]);
		foreach ($paramsExtras as $paramsExtra) $query->andFilterWhere($paramsExtra);

		$query->andFilterWhere(['or',
			['like', 'pegawai.nama', $this->search],
			['like', 'perusahaan.nama', $this->search],
			['like', 'departemen.nama', $this->search],
			['like', 'jabatan.nama', $this->search],
		]);

		return $dataProvider;
	}

}
