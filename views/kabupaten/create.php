<?php
/* @var $this yii\web\View */
/* @var $model app\models\wilayah */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Kabupaten / Kota', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wilayah-create">
    <?= $this->render('_form', [
        'model' => $model,
        'prov_opts' => $prov_opts,
    ]) ?>
</div>
