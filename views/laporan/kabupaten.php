<?php

use yii\helpers\Url;

use yii\grid\GridView;
use kartik\select2\Select2;

$href_redirect = Url::toRoute(['kabupaten', 'pk_parent' => $pk_parent]);
?>


<?php if ($prov_item): ?>
    <h3>
        Laporan banyak penduduk tiap Kabupaten di Provinsi <?= $prov_item->urai ?>
    </h3>
<?php endif ?>


<?= Select2::widget([
    'value' => $pk_parent,
    'name' => 'pk_parent',
    'data' => $prov_list,
    'options' => ['placeholder' => 'Pilih Kabupaten / Kota'],
    'pluginEvents' => [
        "change" => <<<JAVASCRIPT
        function() {
            window.location.replace('${href_redirect}'.replace('${pk_parent}', this.value));
        }
        JAVASCRIPT,
    ],
])
?>

<br>

<?= GridView::widget([
    'dataProvider' => $summary,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'urai',
        [
            'attribute' => 'banyak', 
            'format'=> 'currency',
            'contentOptions' => [
                'style' => 'text-align: right;',
            ]
        ],
    ],
]); ?>
