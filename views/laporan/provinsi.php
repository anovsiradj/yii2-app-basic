<?php

use yii\grid\GridView;

?>

<h1>Laporan Banyak Penduduk Semua Provinsi</h1>

<?= GridView::widget([
    'dataProvider' => $summary,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'urai',
        [
            'attribute' => 'total', 
            'format'=> 'currency', 
            // 'format'=> ['decimal',2], 
            'contentOptions' => ['style' => 'text-align: right;'],
        ],
    ],
]); ?>
