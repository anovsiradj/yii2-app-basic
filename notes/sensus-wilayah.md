
contents didapatkan dari
<https://id.wikipedia.org/wiki/Daftar_kota_di_Indonesia_menurut_jumlah_penduduk>
yang diekstrak menggunakan 
<https://wikitable2csv.ggor.de/>

lokasi contents  `./migrations/sensus-wilayah.csv`

# membuat migrasi

```sh
./yii migrate/create wilayah
./yii migrate/create sensus
./yii migrate/fresh
```

buat skrip untuk generate contents sensus-wilayah, 
yang berada di `./commands/SensusController.php`

```sh
# generate database contents
./yii sensus
```


# membuat MVC

untuk `view` sudah dibuat otomatis oleh `controller`

```sh
# models
./yii gii/model --tableName=wilayah --modelClass=wilayah
./yii gii/model --tableName=sensus --modelClass=sensus

# controllers
./yii gii/crud --controllerClass=app\\controllers\\ProvinsiController --modelClass=app\\models\\wilayah
./yii gii/crud --controllerClass=app\\controllers\\KabupatenController --modelClass=app\\models\\wilayah
./yii gii/controller --controllerClass=app\\controllers\\LaporanController
```
