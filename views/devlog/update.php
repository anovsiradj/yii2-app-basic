<?php
/* @var $this yii\web\View */
/* @var $model app\models\posts */

$this->title = 'Update: ' . ($model->head ?? $model->pk);
$this->params['breadcrumbs'][] = ['label' => 'DevLog', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->head ?? $model->pk, 'url' => ['view', 'id' => $model->pk]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posts-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
