<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\FileHelper;
use yii\helpers\Markdown;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout($page = null)
    {
        $mdTitle = null;
        $mdContent = null;

        $mdNotes = FileHelper::findFiles(Yii::getAlias("@app/notes"), ['only' => ['*.md']]);
        $mdNotes = array_map(fn($i) => explode(DIRECTORY_SEPARATOR, $i), $mdNotes);
        $mdNotes = array_map(fn($i) => end($i), $mdNotes);
        $mdNotes = array_map(fn($i) => ['text' => $i, 'href' => str_replace('.md', '', $i)], $mdNotes);

        if (empty($page)) {
            $pagePath = Yii::getAlias('@app/README.md');
        } else {
            $mdTitle = "${page}.md";
            $pagePath = Yii::getAlias("@app/notes/${page}.md");
        }

        if ($pagePath && file_exists($pagePath) && $content = file_get_contents($pagePath)) {
            $mdContent = Markdown::process($content, 'gfm');
        } else {
            throw new \yii\web\NotFoundHttpException;
        }

        return $this->render('about', [
            'mdTitle' => $mdTitle,
            'mdContent' => $mdContent,
            'mdNotes' => $mdNotes,
        ]);
    }
}
