
membuat simple blog menggunakan yii2 (ekspektasi fitur):
- post list
- post item
- post form (create,update)
- post delete
- user login
- user logout

```sh
./yii migrate/create devlog
./yii migrate

# gawe tables
./yii gii/model --tableName=users --modelClass=Users
./yii gii/model --tableName=posts --modelClass=Posts

./yii gii/crud --controllerClass=app\\controllers\\DevlogController --modelClass=app\\models\\Posts
```

karena ndak tau harus bagaimana. jadi saya tulis disini saja.
default user, supaya bisa insert post.

```sql 
insert into `users` (`usr`,`pwd`,`mail`) select
	'anov',
	'$2y$13$L9ldfmM3Z7rFec9BVfMPDORu2w1.1fldJPqqRuPo0FlrL0tI.Rmgm',
	'anov@mail.com'
	from dual
where not exists (SELECT * from `users`)
```
