<?php

namespace app\models;

class Kabupaten extends Wilayah
{
    /**
     * Gets query for [[Provinsi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvinsi()
    {
        return $this->hasOne(Provinsi::class, ['pk' => 'pk_parent']);
    }
}