<?php

use yii\db\Migration;

/**
 * Class m210209_094314_wilayah
 */
class m210209_094314_wilayah extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wilayah', [
            'pk' => $this->bigPrimaryKey(),
            'pk_parent' => $this->bigInteger()->null(),
            'urai' => $this->string()->notNull(),
            'created' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'updated' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], 'ENGINE=InnoDB');

        $this->addForeignKey(
            'wilayah_pk_parent_fk',
            'wilayah',
            'pk_parent',
            'wilayah',
            'pk',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('wilayah');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_094314_wilayah cannot be reverted.\n";

        return false;
    }
    */
}
