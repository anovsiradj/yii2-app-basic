<?php

namespace app\controllers;

use Yii;
use app\models\wilayah;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KabupatenController implements the CRUD actions for wilayah model.
 */
class KabupatenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all wilayah models.
     * @return mixed
     */
    public function actionIndex($pk_parent = 0)
    {
        $provinsi = wilayah::find()->where(['pk_parent' => null])->asArray(true)->all();
        $provinsi = array_column($provinsi, 'urai', 'pk');

        $dataProvider = new ActiveDataProvider([
            'query' => wilayah::find()->where(['pk_parent' => $pk_parent]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'pk_parent' => $pk_parent,
            'provinsi' => $provinsi,
        ]);
    }

    /**
     * Displays a single wilayah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new wilayah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new wilayah();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk]);
        }

        $prov_opts = wilayah::find()->where(['pk_parent' => null])->all();
        return $this->render('create', [
            'model' => $model,
            'prov_opts' => $prov_opts,
        ]);
    }

    /**
     * Updates an existing wilayah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk]);
        }

        $prov_opts = wilayah::find()->where(['pk_parent' => null])->all();
        return $this->render('update', [
            'model' => $model,
            'prov_opts' => $prov_opts,
        ]);
    }

    /**
     * Deletes an existing wilayah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the wilayah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return wilayah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = wilayah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
