<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\wilayah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wilayah-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pk_parent')->dropDownList(ArrayHelper::map($prov_opts, 'pk', 'urai')) ?>

    <?= $form->field($model, 'urai')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
