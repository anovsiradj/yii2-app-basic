<?php

use yii\db\Migration;

/**
 * Class m210209_094320_sensus
 */
class m210209_094320_sensus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sensus', [
            'pk' => $this->bigPrimaryKey(),
            'pk_wilayah' => $this->bigInteger()->null(),
            'banyak' => $this->bigInteger()->notNull(),
            'created' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'updated' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], 'ENGINE=InnoDB');

        $this->addForeignKey(
            'sensus_wilayah_fk',
            'sensus',
            'pk',
            'wilayah',
            'pk',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sensus');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_094320_sensus cannot be reverted.\n";

        return false;
    }
    */
}
