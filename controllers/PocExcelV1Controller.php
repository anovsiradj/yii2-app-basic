<?php

namespace app\controllers;

use app\classes\ExcelExport;
use app\classes\ExcelImport;
use app\models\PegawaiSearch;
use yii\helpers\Url;

class PocExcelV1Controller extends \yii\web\Controller
{
	public function actionIndex()
	{
		return $this->render('index');
	}

	function actionExport()
	{
		$searchModel = new PegawaiSearch;
		$dataProvider = $searchModel->search($this->request->queryParams);

		$excel = new ExcelExport([
			'templatePath' => '@app/views/poc-excel-v1/template.xlsx',
		]);
		$excel->export($searchModel, $dataProvider);

		return $this->response->sendStreamAsFile($excel->templateBuffer, $excel->templateName, [
			'mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'inline' => false,
		]);
	}


	function actionImport()
	{
		$model = new ExcelImport;
		if ($this->request->isPost && $model->load($this->request->post())) {
			$model->upload_import = \yii\web\UploadedFile::getInstance($model, 'upload_import');
			if($this->request->isAjax) {
				$this->response->format = $this->response::FORMAT_JSON;
				return \yii\widgets\ActiveForm::validate($model);
			}

			if ($model->validate()) {
				$model->import($this->request->queryParams);
			}

			\Yii::$app->session->setFlash('noticeSuccess', "Berhasil import {$model->templateImportedCount} dari {$model->templateImportCount} data pegawai.");
			\Yii::$app->session->setFlash('importHtmlErrors', $model->importHtmlErrors);

			return $this->redirect(['import']);
		}

		return $this->render('import', ['model' => $model]);
	}
}
