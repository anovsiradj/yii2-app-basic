<?php

namespace app\classes;

trait ModelSearch
{
	public $search;

	public function formName()
	{
		return 'filter';
	}

	public function rules()
	{
	    return [
	        [$this->attributes() + ['search'], 'safe'],
	    ];
	}

    public function scenarios() {
    	/** @ignore */
    	return \yii\base\Model::scenarios();
    }

	/**
	 * JIKA data = {kategori: X, jenis: Y, term: Z};
	 * LALU pada modelSearch terdapat attrs/props: {id_kategori_data, id_jenis_data, term};
	 * MAKA {modelSearch.id_kategori_data: params.kategori, modelSearch.id_jenis_data: params.jenis, modelSearch.term: params.term};
	 * 
	 * @param array $data
	 * @return void
	 */
	public function loadFlexible($data)
	{
		/*
			list keys yang sudah diset.
			jangan reset jika exact key sudah diset.
			misal: "tahun" = "tahun", maka jangan set "tahun_ke".
		*/
		$setted = [];

		foreach ($data as $k => $v) {
			if (is_array($k) || is_array($v)) continue;

			if ($this->hasProperty($k) && $this->canSetProperty($k)) {
				$this->{$k} = $v;
				$setted[] = $k;
				continue;
			}

			foreach ($this->attributes() as $attr) {
				if ($attr === $k || (strlen($k) >= 3 && strpos($attr, $k) !== false)) {
					if (!in_array($attr, $setted)) {
						$this->{$attr} = $v;
						$setted[] = $attr;
					}
				}
			}
		}
	}

	/**
	 * @param array $data
	 * @param $formName string|null	
	 * @return bool
	 */
	public function load($data, $formName = null)
	{
		$this->loadFlexible($data);
		return parent::load($data, $formName);
	}
}
