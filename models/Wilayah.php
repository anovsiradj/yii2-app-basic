<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wilayah".
 *
 * @property int $pk
 * @property int|null $pk_parent
 * @property string $urai
 * @property string $created
 * @property string $updated
 *
 * @property wilayah $pkParent
 * @property wilayah[] $wilayahs
 */
class Wilayah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wilayah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pk_parent'], 'integer'],
            [['urai'], 'required'],
            [['created', 'updated'], 'safe'],
            [['urai'], 'string', 'max' => 255],
            [['pk_parent'], 'exist', 'skipOnError' => true, 'targetClass' => wilayah::className(), 'targetAttribute' => ['pk_parent' => 'pk']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pk' => 'Pk',
            'pk_parent' => 'Pk Parent',
            'urai' => 'Urai',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * Gets query for [[PkParent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPkParent()
    {
        return $this->hasOne(Wilayah::className(), ['pk' => 'pk_parent']);
    }

    /**
     * Gets query for [[Wilayahs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWilayah()
    {
        return $this->hasMany(Wilayah::className(), ['pk_parent' => 'pk']);
    }
}
