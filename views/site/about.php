<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
if ($mdTitle) {
	$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['about']];
	$this->params['breadcrumbs'][] = $mdTitle;
} else {
	$this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="site-about">
	<article><?= $mdContent ?></article>

	<br>
	<?php foreach ($mdNotes as $i): ?>
		<?= Html::a($i['text'], ['about', 'page' => $i['href']]) ?>
	<?php endforeach ?>
</div>
