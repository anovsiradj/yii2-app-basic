
Belajar Yii2 <https://github.com/yiisoft/yii2-app-basic>
mulai dari `null`

### instalasi

```sh
git clone git@gitlab.com:anovsiradj/yii2-app-basic.git
cd yii2-app-basic

# dependencies
composer update

# membuat files konfigurasi (untuk isiannya lihat dibawah)
touch ./config/db.php
touch ./config/params.php
touch ./web/index.php

# generate database tables
./yii migrate

# generate database contents untuk wilayah-sensus
./yii sensus

# localhost:8080
./yii serve
```

contoh isian untuk `./config/db.php`

```php
return array_merge(require __DIR__ . '/db_default.php', [
	'dsn' => 'mysql:host=localhost;dbname=?', // disesuaikan
	'username' => '?', // disesuaikan
	'password' => '?', // disesuaikan
]);
```

contoh isian untuk `./config/params.php`

```php
return array_merge(require __DIR__ . '/params_default.php', [
	'analytics' => [
		...
	],
]);
```

contoh isian untuk `./web/index.php`

```php
defined('YII_DEBUG') or define('YII_DEBUG', true); // disesuaikan
defined('YII_ENV') or define('YII_ENV', 'dev'); // disesuaikan

require __DIR__ . '/index_default.php';
```

semua semua materi belajar, mengikuti 
<https://www.yiiframework.com/doc/guide/2.0/en>
secara khusyuk.

<hr>

<p style="text-align: center;">
	2021 &horbar; Powered By 
	<a href="https://www.jmc.co.id/">JMC IT Consultant</a>
</p>
