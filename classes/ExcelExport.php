<?php

namespace app\classes;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ReaderXlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as WriterXlsx;
use Yii;
use yii\base\Model;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;

class ExcelExport extends \yii\base\Model
{
	protected $templatePath; // '@app/template.xlsx';
	protected $templateName;
	public $templateBuffer;

	public $headerPosition = ['x' => 'A', 'y' => 1];
	public $contentPosition = ['x' => 'A', 'y' => 2];

	protected $columnsFields = [];
	protected $fieldsHeaders = [];

	protected $mapped = [];

	protected Model $searchModel;
	protected BaseDataProvider $dataProvider;

	protected ReaderXlsx $reader;
	protected WriterXlsx $writer;
	protected Spreadsheet $spreadsheet;
	protected Worksheet $sheetMain;

	public function init()
	{
		parent::init();
		$this->reader = new ReaderXlsx;
	}

	public function getTemplatePath()
	{
		return $this->templatePath;
	}
	public function setTemplatePath($templatePath)
	{
		$this->templatePath = $templatePath;
		$this->templateName = basename($templatePath);
	}
	public function getTemplateName()
	{
		return $this->templateName;
	}

	protected function parseHeadersToFields()
	{
		$sheet = $this->sheetMain;
		$model = $this->searchModel;

		$y = $this->headerPosition['y'];
		$x_min = $this->headerPosition['x'];
		$x_max = $sheet->getHighestColumn($y);
		$x = $x_min;

		$fields = [];
		while ($x <= $x_max) {
			$cell = $sheet->getCell($x . $y);
			$data = $cell->getValue();
			$fields[$x] = $data;
			$x++;
		}

		foreach ($fields as $x => $header) {
			$field = null;
			$lowerField = strtolower($header);

			if ($model->hasAttribute($header)) {
				$field = $header;
			} elseif ($model->hasAttribute($lowerField)) {
				$field = $lowerField;
			} else {
				foreach ($model->attributeLabels() as $attrField => $attrLabel) {
					if ($attrLabel === $header) {
						$field = $attrField;
					}
				}
			}

			$fields[$x] = $field;
			$this->columnsFields[$x] = $field;
			$this->fieldsHeaders[$field] = $header;
			$this->mapped[] = ['column' => $x, 'x' => $x, 'field' => $field, 'header' => $header];

			if (strpos($field, '.') !== false) {
				$this->createSheetFromRelated($field, $x);
			}
		}

		return $fields;
	}

	protected function createSheetFromRelated($related, $colNumber)
	{
		$model = $this->searchModel;

		$relatedName = explode('.', $related)[0] ?? null;
		$relatedModel = $model->getRelation($relatedName, false);
		if ($relatedModel) $relatedModel = $relatedModel->modelClass;
		if (empty($relatedModel) || !class_exists($relatedModel)) return;

		$relatedField = explode('.', $related)[1] ?? null;
		if (empty($relatedField)) return;

		// $list = $relatedModel::find()->select([$relatedField])->asArray()->all();
		$list = $relatedModel::find()->select([$relatedField])->column();
		$list = array_chunk($list, 1);
		if (empty($list)) return;

		$spreadsheet = $this->spreadsheet;
		$sheet = $spreadsheet->getSheetByName($this->fieldsHeaders[$related]);
		if (empty($sheet)) {
			$sheet = $spreadsheet->createSheet();
			$sheet->setTitle($this->fieldsHeaders[$related]);
		}

		$sheet->fromArray($list, null, 'A1');
	}

	public function export(Model $searchModel, BaseDataProvider $dataProvider)
	{
		$this->searchModel = $searchModel;
		$this->dataProvider = $dataProvider;

		$dataProvider->pagination = false;
		$models = $dataProvider->models;

		$this->spreadsheet = $this->reader->load(Yii::getAlias($this->templatePath));
		$sheet = $this->sheetMain = $this->spreadsheet->getSheetByName('main');

		$this->parseHeadersToFields();

		$y = $this->contentPosition['y'];
		$number = 1;
		foreach ($models as $model) {
			$x = $this->contentPosition['x'];
			foreach ($this->columnsFields as $field) {
				$data = null;
				if ($field === '#' && empty($data)) $data = $number;
				if ($field && empty($data)) $data = ArrayHelper::getValue($model, $field);

				$cell = $sheet->getCell($x.$y);
				$cell->setValue($data);

				$x++;
			}

			$number++;
			$y++;
		}

		$this->createSheetValidation(); 
		// die;

		$this->templateBuffer = tmpfile();
		$this->writer = new WriterXlsx($this->spreadsheet);
		$this->writer->save($this->templateBuffer);
	}

	protected function createSheetValidation()
	{
		foreach ($this->mapped as $map) {
			$header = $map['header'];

			$sheetSource = $this->spreadsheet->getSheetByName($header);
			if (empty($sheetSource)) continue;

			$x = $map['x'];
			$y_min = $this->contentPosition['y'];
			$y_max = 99999;

			$validation = $this->sheetMain->getCell("$x$y_min")->getDataValidation();
			$validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
			$validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
			$validation->setAllowBlank(false);
			$validation->setShowInputMessage(true);
			$validation->setShowErrorMessage(true);
			$validation->setShowDropDown(true);
			$validation->setFormula1($header.'!$A:$A');

			$this->sheetMain->setDataValidation("$x$y_min:$x$y_max", $validation);
		}
	}
}
