<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\posts */

// paksa IDE untuk bisa deteksi datatype (bukan hal penting).
$model = (fn():\app\models\Posts => $model)();

$item_name = $model->users->name ?? $model->users->usr;
$href_name = Url::toRoute(['index', 'search[by]' => $model->users->id]);

$item_created = $model->created;
$item_created = Yii::$app->formatter->asDatetime($item_created);

$this->title = $model->head;
$this->params['breadcrumbs'][] = ['label' => 'DevLog', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
\yii\web\YiiAsset::register($this);
?>
<div class="posts-view">

    <h1><?= Html::encode($model->head) ?></h1>

    <p>
        Oleh
        <?= Html::a($item_name, $href_name) ?>
        Pada <?= $item_created ?>
    </p>

    <article><?= $model->body ?></article>

    <?php if (!Yii::$app->user->isGuest): ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->pk], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->pk], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif ?>
</div>
