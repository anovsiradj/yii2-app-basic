<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = 'Homepage';
?>
<div class="site-index">
	<div class="body-content">
		<p><a href="<?= Url::toRoute('/provinsi/') ?>">Management Provinsi</a></p>
		<p><a href="<?= Url::toRoute('/kabupaten/') ?>">Management Kabupaten / Kota</a></p>
		<p><a href="<?= Url::toRoute('/laporan/') ?>">Laporan</a></p>
		<hr>
		<p><a href="<?= Url::toRoute('/devlog/') ?>">DevLog</a></p>
		<hr>
		<p><?= Html::a('Proof of Concept (POC) Excel v1', ['/poc-excel-v1/']) ?></p>
		<hr>
		<p><a href="<?= Url::toRoute('/user/') ?>">User Management</a></p>
		<hr>
		<p><a href="<?= Url::toRoute('/google-analytics/') ?>">Google Analytics</a></p>
	</div>
</div>
