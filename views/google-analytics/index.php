<?php
use infoweb\analytics\Analytics;

/*
	i noticed some old widgets is missing:
	- TOTAL_PAGE_VIEWS_PER_SESSION
	- TOTAL_BOUNCE_RATE
	- TOTAL_PERSEN_NEW_SESSIONS
	- TOTAL_ACTIVE_USERS_DAYS
	- TOTAL_ACTIVE_USERS
	- CITIES

	can i know the reason why?
	there is no commit history about it (either creation or removal)
	i am confused, the official have no record about it at all.
*/
?>

<h3>Grafik Pengunjung Aplikasi Website</h3>
<div class="row">
	<?= @Analytics::widget(['dataType' => Analytics::SESSIONS]) ?>
</div>
<br>
<div class="row">
	<div class="col-sm-8">
		<?= @Analytics::widget(['dataType' => Analytics::TOTAL_SESSIONS]) ?>
		<?= @Analytics::widget(['dataType' => Analytics::TOTAL_USERS]) ?>
		<?= @Analytics::widget(['dataType' => Analytics::TOTAL_PAGE_VIEWS]) ?>
        <?= null // Analytics::widget(['dataType' => Analytics::TOTAL_PAGE_VIEWS_PER_SESSION]); ?>
		<?= @Analytics::widget(['dataType' => Analytics::AVERAGE_SESSION_LENGTH]) ?>
        <?= null // Analytics::widget(['dataType' => Analytics::TOTAL_BOUNCE_RATE]); ?>
        <?= null // Analytics::widget(['dataType' => Analytics::TOTAL_PERSEN_NEW_SESSIONS]); ?>
	</div>

	<?= @Analytics::widget(['dataType' => Analytics::VISITORS]) ?>	
</div>
<div class="row">
	<?= null // Analytics::widget(['dataType' => Analytics::TOTAL_ACTIVE_USERS_DAYS]); ?>
</div>
<div class="row">
	<?= null // Analytics::widget(['dataType' => Analytics::TOTAL_ACTIVE_USERS]); ?>
	<?= @Analytics::widget(['dataType' => Analytics::COUNTRIES]) // CITIES ?>
</div>
