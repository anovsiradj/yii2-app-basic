
### infoweb-internet-solutions/yii2-cms-analytics

instalasi sesuai panduan resmi, dengan beberapa perubahan.

abaikan langkah "Import the translations and use category ...",
lihat `/config/web.php` pada
```php
$config['i18n']['translations']['infoweb/analytics'];
```

taruh file `certificate.p12` di `assets/certificate`.

selesai.
