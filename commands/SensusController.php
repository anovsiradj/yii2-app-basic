<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use League\Csv\Reader;
use League\Csv\Statement;
use app\models\wilayah;
use app\models\sensus;


class SensusController extends Controller
{
    public function actionIndex()
    {
        $csv = Reader::createFromPath(__DIR__ . '/../migrations/sensus-wilayah.csv', 'r');
        $csv->setHeaderOffset(0);

        $stmt = Statement::create();

        $q = $stmt->process($csv);
        foreach ($q as $b) {
            $prov = $b['Provinsi'];
            $kkab = $b['Kota'];
            $banyak = $b['Sensus 2010BPS[2]'];
            $banyak = str_replace('.', '', $banyak);
            $banyak = (int) $banyak;

            $pk_prov = wilayah::findOne(['urai' => $prov, 'pk_parent' => null]);
            if ($pk_prov) {
                $pk_prov = $pk_prov->primaryKey;
            } else {
                $pk_prov = new wilayah;
                $pk_prov->pk_parent = null;
                $pk_prov->urai = $prov;
                $pk_prov->save();
                $pk_prov = $pk_prov->primaryKey;
            }

            $pk_kkab = wilayah::findOne(['urai' => $kkab, 'pk_parent' => $pk_prov]);
            if ($pk_kkab) {
                $pk_kkab = $pk_kkab->primaryKey;
            } else {
                $pk_kkab = new wilayah;
                $pk_kkab->pk_parent = $pk_prov;
                $pk_kkab->urai = $kkab;
                $pk_kkab->save();
                $pk_kkab = $pk_kkab->primaryKey;
            }

            $sensus = sensus::findOne(['pk_wilayah' => $pk_kkab]);
            if (empty($sensus)) {
                $sensus = new sensus;
                $sensus->pk_wilayah = $pk_kkab;
                $sensus->banyak = $banyak;
                $sensus->save();
            }
        }

        return ExitCode::OK;
    }
}
