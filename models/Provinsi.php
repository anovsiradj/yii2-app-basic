<?php

namespace app\models;

use Yii;

class Provinsi extends Wilayah
{
    /**
     * Gets query for [[Kabupaten]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKabupaten()
    {
        return $this->hasMany(Kabupaten::class, ['pk_parent' => 'pk']);
    }
}
