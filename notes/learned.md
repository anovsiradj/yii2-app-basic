
# hal-hal yang sudah saya pelajari dari Yii2.

tidak ada konsep <https://12factor.net/config>,
orang awam, pasti akan selalu upload config ke VCS
(regardless confidentiality).

istilah environment menggunakan "dev" dan "prod".

terlalu ketergantungan pada `gii` (koyo `artisan` nggone `laravel`).

oke, saiki aku dong konsep environment-ne.
dadi, ning folder /config/ sing ono suffix `*-local.php`
kui mengko di-**merge** karo sing ora ono suffix-e.
**misal**-e `db.php` bakal di-merge karo `db-local.php`.
urutane, sing ono suffix-e mesti keri dewe, dadi bakal nimpa.
maka-dari-itu, saya mengambil kesimpulan.
file yang ada suffix `*local.php`, tidak usah di commit ke git.

`/config/params*.php` kui config seperti umumnya, bentuke multi-array.
engko key dan value-ne bakal di-inject ning `Yii::$app`.

kakean alias
<https://www.yiiframework.com/extension/yiisoft/yii2-app-advanced/doc/guide/2.0/en/structure-path-aliases>

Gii ada GUI-ne, kalo sangat males nulis2 ning CLI, bisa tinggal click2 ae.
