<?php
/* @var $this yii\web\View */
/* @var $model app\models\wilayah */

$this->title = 'Update: ' . ($model->urai ?? $model->pk);
$this->params['breadcrumbs'][] = ['label' => 'Kabupaten / Kota', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ($model->urai ?? $model->pk), 'url' => ['view', 'id' => $model->pk]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wilayah-update">
    <?= $this->render('_form', [
        'model' => $model,
        'prov_opts' => $prov_opts,
    ]) ?>

</div>
