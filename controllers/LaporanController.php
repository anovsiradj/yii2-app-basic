<?php

namespace app\controllers;

use app\models\wilayah;
use Yii;
use yii\data\ArrayDataProvider;

class LaporanController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProvinsi()
    {
    	$summary = Yii::$app->db->createCommand(<<<SQL
            select sum(banyak) as total,w2.urai
            from sensus
            join wilayah as w1 on (w1.pk = sensus.pk_wilayah and w1.pk_parent is not null)
            join wilayah as w2 on (w2.pk = w1.pk_parent and w2.pk_parent is null)
            group by w2.pk,w2.urai
        SQL)->queryAll();

        $summary = new ArrayDataProvider([
            'allModels' => $summary, 
            'pagination' => false, 
        ]);

        return $this->render('provinsi', [
            'summary' => $summary,
        ]);
    }

    public function actionKabupaten($pk_parent = 0)
    {
        $summary = Yii::$app->db->createCommand(<<<SQL
            select banyak,w1.urai
            from sensus
            join wilayah as w1 on (w1.pk = sensus.pk_wilayah and w1.pk_parent is not null)
            where w1.pk_parent=:pk_parent
        SQL, [
            'pk_parent' => $pk_parent,
        ])->queryAll();

        $summary = new ArrayDataProvider([
            'allModels' => $summary, 
            'pagination' => false, 
        ]);

        $prov_list = wilayah::find()->where(['pk_parent' => null])->asArray(true)->all();
        $prov_list = array_column($prov_list, 'urai', 'pk');
        $prov_item = wilayah::findOne($pk_parent);

        return $this->render('kabupaten', [
            'pk_parent' => $pk_parent,
            'prov_list' => $prov_list,
            'prov_item' => $prov_item,
            'summary' => $summary, 
        ]);
    }
}
