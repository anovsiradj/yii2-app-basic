<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sensus".
 *
 * @property int $pk
 * @property int|null $pk_wilayah
 * @property int $tahun
 * @property string $sumber
 * @property string $banyak
 * @property string $created
 * @property string $updated
 *
 * @property Wilayah $pk0
 */
class Sensus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sensus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pk_wilayah', 'banyak'], 'integer'],
            [['pk_wilayah','banyak'], 'required'],
            [['created', 'updated'], 'safe'],
            [['pk'], 'exist', 'skipOnError' => true, 'targetClass' => Wilayah::className(), 'targetAttribute' => ['pk' => 'pk']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pk' => 'Pk',
            'pk_wilayah' => 'Pk Wilayah',
            'banyak' => 'Banyak',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * Gets query for [[Wilayah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWilayah()
    {
        return $this->hasOne(Wilayah::className(), ['pk' => 'pk']);
    }
}
