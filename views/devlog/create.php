<?php
/* @var $this yii\web\View */
/* @var $model app\models\posts */

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'DevLog', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
