
# instalasi

```sh
composer create-project --prefer-dist yiisoft/yii2-app-basic .

# untuk lihat apa saja, yang belum sesuai dengan yii2.
php requirements.php

# skrip migrasi berada di `./migrations/*.php`
./yii migrate

# localhost:8080
./yii serve
```

### lainnya

simplify contents di `/.composer.json`.
salah-satunya, hapus field "extra" (hanya digunakan sekali, ketika `create-project`).

files yang saya hapus karena tidak digunakan,

```
./.bowerrc
./LICENSE.md
./README.md
./test/
./codeception.yml
./vagrant/
./Vagrantfile
./docker-compose.yml
```

terutama **tests** saya hapus, toh di _real world_ tidak ada yang bisa pakai,
bahkan "mastah" di tempat-kerja kalian.
