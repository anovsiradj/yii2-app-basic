<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kabupaten / Kota';
$this->params['breadcrumbs'][] = $this->title;

$href_redirect = Url::toRoute(['index', 'pk_parent' => $pk_parent]);
?>

<div class="wilayah-index">

    <div class="row">
        <div class="col-md-2">
            <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-10">
            <?= Select2::widget([
                'value' => $pk_parent,
                'name' => 'pk_parent',
                'data' => $provinsi,
                'options' => ['placeholder' => 'Pilih Kabupaten / Kota'],
                'pluginEvents' => [
                    "change" => <<<JAVASCRIPT
                    function() {
                        window.location.replace('${href_redirect}'.replace('${pk_parent}', this.value));
                    }
                    JAVASCRIPT,
                ],
            ])
            ?>
        </div>
    </div>

    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'urai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
