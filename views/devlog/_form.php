<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use skeeks\yii2\ckeditor\CKEditorWidget;
use skeeks\yii2\ckeditor\CKEditorPresets;

/* @var $this yii\web\View */
/* @var $model app\models\posts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'head')
        ->label('Judul')
        ->textInput(['maxlength' => true])
        ?>

    <?= $form
    	->field($model, 'body')
        ->label('Isian')
    	->textarea(['rows' => 7])
    	->widget(CKEditorWidget::class, [
    		'preset' => CKEditorPresets::BASIC,
    	])
        ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
